from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Raniadita Khairunnisa' # TODO Implement this
birth_year = 1999

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return date.today().year - birth_year
